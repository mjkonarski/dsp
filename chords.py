import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal
import pprint as pp
import sys
import math
from scipy.io import wavfile
import pyaudio

np.set_printoptions(threshold=sys.maxsize)

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 1

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

NOTES = {
    "C": 16.35,
    "C#": 17.32,
    "D": 18.35,
    "D#": 19.45,
    "E": 20.60,
    "F": 21.83,
    "F#": 23.12,
    "G": 24.50,
    "G#": 25.96,
    "A": 27.50,
    "A#": 29.14,
    "B": 30.87 
}

NOTES_NAMES = sorted(list(NOTES.keys()))

# Fs, x = wavfile.read("chords/f-mol.wav")

# sys.exit()

while True:
    x = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        data = np.fromstring(data, np.int16)
        x.extend(data)

    X = np.fft.fft(x)
    X2 = np.abs(np.real(X[0:int(len(X)/2)]))

    peaks, _ = scipy.signal.find_peaks(X2, height=1e7, distance=10)
    freqs = np.fft.fftfreq(X.size, d=1/RATE)

    found_notes = set()

    for p in peaks:
        best_note = None
        best_diff = sys.maxsize

        for note in NOTES:
            t = math.log2(freqs[p] / NOTES[note])
            diff = abs(round(t) - t)

            if diff < best_diff:
                best_note = note
                best_diff = diff

        found_notes.add(best_note)

    #print(found_notes)
    print("\n\n\n\n\n\n\n\n\n\n\n\n")

    for i in range(len(NOTES_NAMES)):
        major_chord = set([NOTES_NAMES[i], NOTES_NAMES[(i+4) % len(NOTES_NAMES)], NOTES_NAMES[(i+7) % len(NOTES_NAMES)]])

        if major_chord.issubset(found_notes):
            print("\n\n")
            print(NOTES_NAMES[i] + " major")
            print("\n\n")

        minor_chord = set([NOTES_NAMES[i], NOTES_NAMES[(i+3) % len(NOTES_NAMES)], NOTES_NAMES[(i+7) % len(NOTES_NAMES)]])

        if minor_chord.issubset(found_notes):
            print("\n\n")
            print(NOTES_NAMES[i] + " minor")
            print("\n\n")

    # plt.plot(X2)
    # plt.plot(peaks, X2[peaks], "x")
    # plt.show()

stream.stop_stream()
stream.close()
p.terminate()

